package rs.mpele.pHStat;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

public class MinVremeIzmedjuDodavanjaDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JSpinner spinner;
	boolean bOK = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MinVremeIzmedjuDodavanjaDialog dialog = new MinVremeIzmedjuDodavanjaDialog(30);
			dialog.setVisible(true);
			if(dialog.bOK == true)
				System.out.println(dialog.getMinIzmedjuDvaDodavanja());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Integer getMinIzmedjuDvaDodavanja() {
		Object o = spinner.getValue();
		Number n = (Number) o;
		return n.intValue() ;
	}

	/**
	 * Create the dialog.
	 */
	public MinVremeIzmedjuDodavanjaDialog(long vremeDodavanjaStari) {
		setTitle("Дефинисање минималног времена између додавања");
		setBounds(100, 100, 314, 104);
		setModal(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);;
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						bOK = true;
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		{
			JLabel lblNewLabel = new JLabel("Дефинисање минималног времена између додавања");
			getContentPane().add(lblNewLabel, BorderLayout.NORTH);
		}
		{
			long min = 5;
			long max = 99999;
			long step = 1;
			spinner =new JSpinner(new SpinnerNumberModel(vremeDodavanjaStari, min, max, step));
			spinner.setMinimumSize(new Dimension(150, 20));
			spinner.setPreferredSize(new Dimension(150, 20));
			getContentPane().add(spinner, BorderLayout.WEST);
		}
	}
}
