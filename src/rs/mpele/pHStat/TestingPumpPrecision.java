package rs.mpele.pHStat;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;

public class TestingPumpPrecision extends JFrame {

	private static final long serialVersionUID = 768004439941930891L;
	private JPanel contentPane;
	private JTextField textFieldVolumeToAdd;
	private JTextField textFieldJedinicnaZapremina;
	private JTextField textFieldVoltage;
	private JTextField textFieldAddedVolume;
	protected ArduinoCore mArduinoCore;
	protected int mNumberOfDosing;
	protected double mAddedVolume;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ArduinoCore arduinoCore = new ArduinoCore();
					arduinoCore.konektujSe();

					TestingPumpPrecision frame = new TestingPumpPrecision(arduinoCore);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param arduinoCore 
	 */
	public TestingPumpPrecision(ArduinoCore arduinoCore) {
		this.mArduinoCore = arduinoCore;

		setTitle("Testing pump precision");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][][][][]", "[][][][][][]"));

		JLabel lblVolumeToAdd = new JLabel("Volume to add");
		contentPane.add(lblVolumeToAdd, "flowx,cell 0 0");

		textFieldVolumeToAdd = new JTextField();
		textFieldVolumeToAdd.setText("150");
		contentPane.add(textFieldVolumeToAdd, "cell 0 0");
		textFieldVolumeToAdd.setColumns(10);

		JLabel lblJedinicnaZapremina = new JLabel("Jedinicna zapremina");
		contentPane.add(lblJedinicnaZapremina, "flowx,cell 0 1");

		textFieldJedinicnaZapremina = new JTextField();
		textFieldJedinicnaZapremina.setText("0.5");
		contentPane.add(textFieldJedinicnaZapremina, "cell 0 1");
		textFieldJedinicnaZapremina.setColumns(10);

		JLabel lblMl = new JLabel("ml");
		contentPane.add(lblMl, "cell 0 1");

		JLabel label = new JLabel("ml");
		contentPane.add(label, "cell 0 0");

		JLabel lblWorkingVoltage = new JLabel("Working voltage");
		contentPane.add(lblWorkingVoltage, "flowx,cell 0 2");

		textFieldVoltage = new JTextField();
		textFieldVoltage.setText("9");
		contentPane.add(textFieldVoltage, "cell 0 2");
		textFieldVoltage.setColumns(10);

		JLabel lblV = new JLabel("V");
		contentPane.add(lblV, "cell 0 2");

		JButton btnAdd = new JButton("Start test");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mNumberOfDosing = 0;
				mAddedVolume = 0;
				while(mAddedVolume < Double.valueOf(textFieldVolumeToAdd.getText())){
					mArduinoCore.dozirajVolumeMl(Double.valueOf(textFieldJedinicnaZapremina.getText()));
					
					mNumberOfDosing++;
					mAddedVolume += Double.valueOf(textFieldJedinicnaZapremina.getText());
					System.out.println("Doziranje: "+ mNumberOfDosing+ " - " + mAddedVolume);
					
					try {
						TimeUnit.MILLISECONDS.sleep(1000+mArduinoCore.mL2mS(Double.valueOf(textFieldJedinicnaZapremina.getText())));
					} catch (NumberFormatException | InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		contentPane.add(btnAdd, "cell 0 3,alignx right");

		JLabel lblMeasuredAddedVolume = new JLabel("Measured added volume");
		contentPane.add(lblMeasuredAddedVolume, "flowx,cell 0 4");

		textFieldAddedVolume = new JTextField();
		contentPane.add(textFieldAddedVolume, "cell 0 4");
		textFieldAddedVolume.setColumns(10);

		JLabel label_1 = new JLabel("ml");
		contentPane.add(label_1, "cell 0 4");

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PrintWriter out = null;
				try {
					out = new PrintWriter(new BufferedWriter(new FileWriter("TestingPumpPrecision.txt", true)));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String izlazPodaci = new SimpleDateFormat("yyyy.dd.MM HH.mm.ss").format(new Date())+";"+textFieldVoltage.getText()+";"+textFieldJedinicnaZapremina.getText()+";"+textFieldVolumeToAdd.getText()+";"+textFieldAddedVolume.getText()+";"+mArduinoCore.getKoeficijentPumpe();
				out.println(izlazPodaci);
				out.close();
			}
		});
		contentPane.add(btnSave, "cell 0 5,alignx right");
	}

}
