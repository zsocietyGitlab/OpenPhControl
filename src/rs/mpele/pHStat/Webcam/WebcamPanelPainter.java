package rs.mpele.pHStat.Webcam;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.ParseException;

import com.github.sarxos.webcam.WebcamPanel;

import rs.mpele.pHStat.GraphTimeFrame;
import rs.mpele.pHStat.GraphVolumeFrame;
import rs.mpele.pHStat.Kontrolor;
import rs.mpele.pHStat.Podesavanja;

/**
 *  Ova klasa koordinira citanje cifara sa kamere i njenu obradu
 *
 */
public class WebcamPanelPainter implements WebcamPanel.Painter{
	OcitajPH ocitajPH;
	boolean radiCitanje= false;
	private boolean mUkljucenKontrola = false;
	private long mUcestalostKorekcije = 1000; // u mili sekundama
	private long poslednjaObrada = 0;
	private Kontrolor mKontrolor;
	private CalibrationCammeraLive kalibracijaKamereLive;
	private GraphTimeFrame mGrafikTimeFrame;
	private GraphVolumeFrame mGraphVolumeFrame;

	private boolean simulator = false;

	public WebcamPanelPainter(OcitajPH pOcitajPH, Kontrolor kontrolor, GraphTimeFrame frame, GraphVolumeFrame frame2) throws IOException{
		System.out.println("Inicijalizacija OcitajPH");
		this.ocitajPH = pOcitajPH;
		this.mKontrolor = kontrolor; 
		this.mGrafikTimeFrame = frame;
		this.mGraphVolumeFrame = frame2;
		this.kalibracijaKamereLive = new CalibrationCammeraLive(ocitajPH);

	}

	/**
	 * Ucitava podesavanja direktno iz fajla
	 */
	public void setPodesavanja(){
		ocitajPH.ucitajPodesavanja();
	}

	public void setPodesavanja(Podesavanja podesavanja){
		ocitajPH.ucitajPodesavanja(podesavanja);
	}

	/**
	 * Ukljucuje citanje postavljene vrednosti
	 */
	public void setRadiCitanje(){
		radiCitanje= true;
	}

	public void ukljuciKontrolu(){
		mUkljucenKontrola = true;
	}
	public void iskljuciKontrolu(){
		mUkljucenKontrola = false;
	}

	public boolean daLiJeUkljucenaKontrola(){
		return mUkljucenKontrola;
	}

	public void ukljuciKalibracijuKamere(){
		this.kalibracijaKamereLive.setVisible(true);		
	}

	public void paintImage(WebcamPanel panel, BufferedImage image, Graphics2D g2) {

		//		MarvinImage mBWMarviImage = new MarvinImage(image);
		//		mBWMarviImage.update(); // Ne znam sta je u pitanju, ali bez ovoga nece da radi !!!
		//		thresholding(mBWMarviImage, 38);
		//		mBWMarviImage.update(); // Ne znam sta je u pitanju, ali bez ovoga nece da radi !!!
		//		//mBWMarviImage.drawRect(0,0,100,100, 30, Color.RED);
		//		BufferedImage image2 = mBWMarviImage.getBufferedImage();

		if(radiCitanje == true){
			if(simulator == false){
				ocitajPH.setBufferedImageSaKamere(image);
				double procitanPH = ocitajPH.obradi();
				panel.getDefaultPainter().paintImage(panel, ocitajPH.nacrtajOkvir(), g2);
				long now = System.currentTimeMillis(); 
				try {
					this.kalibracijaKamereLive.setSlika(ocitajPH.getPrepoznavanjeCifre().getRadnaKopijaMarvinImage());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(ocitajPH.daLiJeValidnoCitanje()){
					if(mUkljucenKontrola && now - poslednjaObrada  > mUcestalostKorekcije){
						System.out.println("Ocitano: "+ procitanPH);
						try {
							mKontrolor.obradi(procitanPH);
							mGrafikTimeFrame.dodajVrednosti(mKontrolor.getIzlazPodaci());
							mGraphVolumeFrame.dodajVrednostiDodataZapremina(mKontrolor.getIzlazPodaci());
						} catch (ParseException e) {
							e.printStackTrace();
						}
						System.out.println("Do sada dodato: "+mKontrolor.getUkupnoDodatoMililitara()+" ml NaOH");
						poslednjaObrada = now;
					}
				}
				else {
					System.out.println("Lose citanje !!!");
				}
			}
			else { // simulator
				double procitanPH = ocitajPH.obradi();
				mKontrolor.obradi(procitanPH);
				try {
					mGrafikTimeFrame.dodajVrednosti(mKontrolor.getIzlazPodaci());
					mGraphVolumeFrame.dodajVrednostiDodataZapremina(mKontrolor.getIzlazPodaci());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		else{			
			panel.getDefaultPainter().paintImage(panel, image, g2);
		}
	}


	public void paintPanel(WebcamPanel panel, Graphics2D g2) {
		panel.getDefaultPainter().paintPanel(panel, g2);
	}

}
