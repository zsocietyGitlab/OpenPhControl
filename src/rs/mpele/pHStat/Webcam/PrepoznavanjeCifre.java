package rs.mpele.pHStat.Webcam;

import static marvin.MarvinPluginCollection.thresholding;
import static marvin.MarvinPluginCollection.colorChannel;

import java.util.HashMap;
import java.util.Map;

import marvin.image.MarvinImage;

public class PrepoznavanjeCifre {

	private int sirinaCifre;
	private int visinaCifre;
	private double debljinaSegmenta=0.15;
	private int mTreshold = 125;
	private Map <String,Integer> mSegmentiMap = new HashMap<String,Integer>();
	private boolean mDebug=false;

	MarvinImage mOriginalMarvinImage;
	MarvinImage mRadnaKopijaMarvinImage;


	public PrepoznavanjeCifre() { 
		//raspored segmenata je sa http://www.pyimagesearch.com/2017/02/13/recognizing-digits-with-opencv-and-python/
		mSegmentiMap.put("0000000", 0);
		mSegmentiMap.put("1110111", 0);
		mSegmentiMap.put("0010010", 1);
		mSegmentiMap.put("1011101", 2);
		mSegmentiMap.put("1011011", 3);
		mSegmentiMap.put("0111010", 4);
		mSegmentiMap.put("1101011", 5);
		mSegmentiMap.put("1101111", 6);
		mSegmentiMap.put("1010010", 7);
		mSegmentiMap.put("1111111", 8);
		mSegmentiMap.put("1111011", 9);
	}

	public void setSirinaCifre(int sirinaCifre) {
		this.sirinaCifre = sirinaCifre;
	}

	public void setVisinaCifre(int visinaCifre) {
		this.visinaCifre = visinaCifre;
	}

	public void setDebljinaSegmenta(double visinaHorizonatalnogSegmenta2) {
		this.debljinaSegmenta = visinaHorizonatalnogSegmenta2;
	}


	/**
	 * Obradjuje crno belu MarvinImage sliku
	 * @param image
	 * @param x
	 * @param y
	 * @return
	 */
	public int obradi(int x, int y) {
		//mRadnaKopijaMarvinImage = image.clone();
		String tmp = "";
		tmp = tmp + (daLiJeUkljucenSegment(mRadnaKopijaMarvinImage, 	(int) (x+sirinaCifre*.35),  y, 							(int) (sirinaCifre*.45), 									(int) (visinaCifre*debljinaSegmenta))? "1" : "0");
		tmp = tmp + (daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.3),  (int) (y+visinaCifre*0.1), 	(int) (x+sirinaCifre*debljinaSegmenta), 					(int) (y+visinaCifre*.45)			)? "1" : "0");
		tmp = tmp + (daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.9),  (int) (y+visinaCifre*0.1), 	(int) (x+sirinaCifre*debljinaSegmenta+sirinaCifre*.6), 		(int) (y+visinaCifre*.45)			)? "1" : "0");
		tmp = tmp + (daLiJeUkljucenSegment(mRadnaKopijaMarvinImage, 	(int) (x+sirinaCifre*.25),  (int) (y+visinaCifre*0.42), (int) (sirinaCifre*.45), 									(int) (visinaCifre*debljinaSegmenta))? "1" : "0");
		tmp = tmp + (daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.15), (int) (y+visinaCifre*0.55), 	(int) (x+sirinaCifre*debljinaSegmenta*.25), 				(int) (y+visinaCifre*.95)			)? "1" : "0");
		tmp = tmp + (daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.8),  (int) (y+visinaCifre*0.55), 	(int) (x+sirinaCifre*debljinaSegmenta*.25+sirinaCifre*.6), 	(int) (y+visinaCifre*.95)			)? "1" : "0");
		tmp = tmp + (daLiJeUkljucenSegment(mRadnaKopijaMarvinImage, 	(int) (x+sirinaCifre*.13), 	(int) (y+visinaCifre*0.85),	(int) (sirinaCifre*.45),	 									(int) (visinaCifre*debljinaSegmenta))? "1" : "0");

		//		if(mDebug){  Nije azurirano !!!
		//			System.out.println("segment 0: "+daLiJeUkljucenSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.2), y, (int) (sirinaCifre*.8), (int) (visinaCifre*debljinaSegmenta)));
		//			System.out.println("segment 1: "+daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.3), (int) (y+visinaCifre*0.05), (int) (x+sirinaCifre*debljinaSegmenta), (int) (y+visinaCifre*.5)));
		//			System.out.println("segment 2: "+daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.9), (int) (y+visinaCifre*0.05), (int) (x+sirinaCifre*debljinaSegmenta+sirinaCifre*.6), (int) (y+visinaCifre*.5)));
		//			System.out.println("segment 3: "+daLiJeUkljucenSegment(mRadnaKopijaMarvinImage, x, (int) (y+visinaCifre*0.42), sirinaCifre, (int) (visinaCifre*debljinaSegmenta)));
		//			System.out.println("segment 4: "+daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.15), (int) (y+visinaCifre*0.55), (int) (x+sirinaCifre*debljinaSegmenta*.25), (int) (y+visinaCifre*.95)));
		//			System.out.println("segment 5: "+daLiJeUkljucenKosiSegment(mRadnaKopijaMarvinImage, (int) (x+sirinaCifre*.8), (int) (y+visinaCifre*0.55), (int) (x+sirinaCifre*debljinaSegmenta*.25+sirinaCifre*.6), (int) (y+visinaCifre*.95)));
		//			System.out.println("segment 6: "+daLiJeUkljucenSegment(mRadnaKopijaMarvinImage, x, (int) (y+visinaCifre*0.85), (int) (sirinaCifre*.8), (int) (visinaCifre*debljinaSegmenta)));
		//		}

		//System.out.println("Ocitani segmenti:" +tmp);
		Integer rez = mSegmentiMap.get(tmp); 
		if(rez == null){
			rez = -1;
		}

		return rez;
	}

	private boolean daLiJeUkljucenKosiSegment(MarvinImage image, int x1, int y1, int x2, int y2) {
		int brojac=0;
		int brojacUkupno=0;
		for(int iy = y1;iy<y2;iy++){
			for(int ix = (int) (x1+(x2-x1)*(iy-y1)/(y2-y1)-sirinaCifre*debljinaSegmenta*.5);ix<(int) (x1+(x2-x1)*(iy-y1)/(y2-y1)+sirinaCifre*debljinaSegmenta*.5);ix++){
				brojacUkupno++;
				int boja = image.getIntComponent0(ix, iy);
				if(boja!=0) { // ispituje crvenu komponentu boje
					brojac++;
					image.setIntColor(ix, iy, 0, 255, 0);
				}
				else{
					image.setIntColor(ix, iy, 0, 0, 255);
				}
				//System.out.println(image.getIntComponent0(ix, iy));
			}
		}
		if(mDebug){			
			System.out.println(" "+brojac+"/"+brojacUkupno+" "+ (double)brojac/brojacUkupno);
		}
		return ( ((double)brojac/brojacUkupno > 0.4) ? true : false);

	}

	private boolean daLiJeUkljucenSegment(MarvinImage image, int x, int y, int sirina, int visina){
		int brojac=0;
		int brojacUkupno=0;
		for(int ix = x;ix<x+sirina;ix++){
			for(int iy = y;iy<y+visina;iy++){
				brojacUkupno++;
				if(image.getIntComponent0(ix, iy)!=0) { // ispituje crvenu komponentu boje
					brojac++;
					image.setIntColor(ix, iy, 0, 255, 0);
				}
				else{
					image.setIntColor(ix, iy, 0, 0, 255);
				}
				//System.out.println(image.getIntComponent0(ix, iy));
			}
		}
		if(mDebug){
			System.out.println(sirina*visina+" "+brojac+"/"+brojacUkupno+" "+ (double)brojac/brojacUkupno);
		}
		return ( ((double)brojac/brojacUkupno > 0.5) ? true : false);
	}



	public MarvinImage getRadnaKopijaMarvinImage() {
		return mRadnaKopijaMarvinImage;
	}

	public void setSliku(MarvinImage pMarviImage) {
		mOriginalMarvinImage = pMarviImage.clone();
		mRadnaKopijaMarvinImage = pMarviImage.clone();
		threshold();
	}

	public int getTreshold() {
		return mTreshold;
	}

	/**
	 * Postavlja treshold i azurira treshold sliku
	 * @param treshold
	 */
	public void setTreshold(int treshold) {
		this.mTreshold = treshold;

		if(mOriginalMarvinImage==null)
			return;

		mRadnaKopijaMarvinImage = mOriginalMarvinImage.clone();
		threshold();
		//System.out.println(mOriginalMarvinImage.getWidth()+"*"+mOriginalMarvinImage.getHeight());
		//System.out.println(mRadnaKopijaMarvinImage.getWidth()+"*"+mRadnaKopijaMarvinImage.getHeight());
	}

	
	private void threshold(){
		colorChannel(mRadnaKopijaMarvinImage, mRadnaKopijaMarvinImage, 0, -200, -200);// brise zelenu i plavu komponentu
		thresholding(mRadnaKopijaMarvinImage, mTreshold);
		//thresholdingNeighborhood(mOriginalMarvinImage.clone(), mRadnaKopijaMarvinImage, .8, 8, 1);
	}

}
