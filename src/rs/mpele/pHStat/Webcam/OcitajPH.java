package rs.mpele.pHStat.Webcam;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import marvin.image.MarvinImage;
import rs.mpele.pHStat.Podesavanja;

public class OcitajPH {
	private BufferedImage mBufferedImageSaKamere;
	private MarvinImage mMarviImage;
	private PrepoznavanjeCifre prepoznavanjeCifre;
	private int x;
	private int y;
	private int sirina;
	private int visina;
	private double prethodnaTackaRelativno;

	private Integer[] mCifreOcitane = new Integer[5];
	private int mBrojCifara = 5;
	private int mBrojZnacajnihCifaraIzaZareza = 2;


	public static void main(String[] args) {
		OcitajPH tmp = new OcitajPH();
		tmp.mCifreOcitane[0] = -1;
		tmp.mCifreOcitane[1] = -1;
		tmp.mCifreOcitane[2] = 3;
		tmp.mCifreOcitane[3] = 4;
		tmp.mCifreOcitane[4] = 5;
		System.out.println(tmp.daLiJeValidnoCitanje());
		System.out.println(tmp.ocitanaVrednost());
	}

	public OcitajPH() {
		prepoznavanjeCifre = new PrepoznavanjeCifre();

		ucitajPodesavanja();
	}

	public BufferedImage getmBufferedImageSaKamere() {
		return mBufferedImageSaKamere;
	}

	public void setBufferedImageSaKamere(BufferedImage pBufferedImageSaKamere) {
		this.mBufferedImageSaKamere = pBufferedImageSaKamere;
		this.mMarviImage = new MarvinImage(this.mBufferedImageSaKamere);
		//System.out.println("pBufferedImageSaKamere"+pBufferedImageSaKamere.getHeight()+"x"+pBufferedImageSaKamere.getWidth());
		//System.out.println("mMarviImage"+mMarviImage.getHeight()+"x"+mMarviImage.getWidth());
	}

	public PrepoznavanjeCifre getPrepoznavanjeCifre() {
		return prepoznavanjeCifre;
	}

	public void setPrepoznavanjeCifre(PrepoznavanjeCifre prepoznavanjeCifre) {
		this.prepoznavanjeCifre = prepoznavanjeCifre;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSirina() {
		return sirina;
	}

	public void setSirina(int sirina) {
		this.sirina = sirina;
	}

	public int getVisina() {
		return visina;
	}

	public void setVisina(int visina) {
		this.visina = visina;
	}

	public double getPrethodnaTackaRelativno() {
		return prethodnaTackaRelativno;
	}

	public void setPrethodnaTackaRelativno(double prethodnaTackaRelativno) {
		this.prethodnaTackaRelativno = prethodnaTackaRelativno;
	}


	public Double obradi(){
		getPrepoznavanjeCifre().setSirinaCifre(getSirina());
		getPrepoznavanjeCifre().setVisinaCifre(getVisina());

		if(mMarviImage==null){
			System.out.println("Preskace obradu ovog frejma");
			return null;
		}

		getPrepoznavanjeCifre().setSliku(mMarviImage);
		for(int i = 0;i<mBrojCifara;i++){
			mCifreOcitane[i] = getPrepoznavanjeCifre().obradi(getXzaCifru(i), getY());
		}
		//System.out.println("Ocitano: "+ ocitanaVrednost());
		return ocitanaVrednost();
	}

	/**
	 * Vraca kopiju slike sa nacrtanim okvirom
	 * @return
	 */
	public BufferedImage nacrtajOkvir(){
		// Ovde radi samo ako je definisano 1 za tip slike - nece ako kopira sa originalne slike
		BufferedImage b = new BufferedImage(this.getmBufferedImageSaKamere().getWidth(), this.getmBufferedImageSaKamere().getHeight(), 1);

		Graphics g = b.getGraphics();
		g.drawImage(this.getmBufferedImageSaKamere(), 0, 0, null);
		((Graphics2D) g).setStroke(new BasicStroke(5)); // ne shvatam zasto je potreban ovaj casting


		int fontSize = getVisina();
		g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));

		for(int i = 0;i<mBrojCifara;i++){
			g.setColor(Color.GREEN);
			g.drawRect(getXzaCifru(i), this.getY(), this.getSirina(), this.getVisina());
			if(mCifreOcitane[i]== -1)
				g.setColor(Color.RED);
			g.drawString(String.valueOf(mCifreOcitane[i]), getXzaCifru(i), getY());
		}

		g.dispose();
		return b;		
	}


	/**
	 * Vraca gornju levu koordinatu cifre 
	 *   	cifre se broje unazad pocevsi od 0 
	 * @param cifra
	 * @return
	 */
	private int getXzaCifru(int cifra){
		return (int) (getX()-cifra*getSirina()*getPrethodnaTackaRelativno());
	}



	public void ucitajPodesavanja() {
		Podesavanja podesavanja = new Podesavanja();
		try {
			podesavanja.ucitajPodesavanja();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ucitajPodesavanja(podesavanja);
	}

	public void ucitajPodesavanja(Podesavanja podesavanja) {
		Properties prop = podesavanja.getProperties();

		this.setX(Integer.valueOf(prop.getProperty("x")));
		this.setY(Integer.valueOf(prop.getProperty("y")));
		this.setSirina(Integer.valueOf(prop.getProperty("sirina")));
		this.setVisina(Integer.valueOf(prop.getProperty("visina")));
		this.setPrethodnaTackaRelativno(Double.valueOf(prop.getProperty("prethodnaTackaRelativno","1.2")));

		getPrepoznavanjeCifre().setTreshold(Integer.valueOf(prop.getProperty("treshold")));
	}


	public Double ocitanaVrednost(){
		Double ocitanaVrednost = 0.;
		for(int i = 3-mBrojZnacajnihCifaraIzaZareza;i<mBrojCifara;i++){
			ocitanaVrednost += Math.pow(10, i-mBrojCifara +2) * mCifreOcitane[i];
		}
		return ocitanaVrednost;
	}

	public boolean daLiJeValidnoCitanje(){
		for(int i = 3-mBrojZnacajnihCifaraIzaZareza;i<mBrojCifara;i++){
			if(mCifreOcitane[i] == -1)
				return false;
		}
		return true;
	}
}